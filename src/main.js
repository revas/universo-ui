import RevasUI from '@revas/ui/vue'

import universoStore from '@/modules/universo/universo.store'
import universoRoutes from '@/modules/universo/universo.router'

const routes = [].concat(universoRoutes)

const options = {
  base: {
    domain: process.env.VUE_APP_BASE_DOMAIN,
    host: process.env.VUE_APP_BASE_HOST,
    api: {
      url: process.env.VUE_APP_BASE_API_URL
    }
  },
  router: {
    default: {
      name: 'UNIVERSO.APP_COLLECTION.$NAME',
      require: {
        realm: true
      }
    }
  },
  auth: {
    oauth2: {
      domain: process.env.VUE_APP_AUTH_OAUTH2_DOMAIN,
      clientID: process.env.VUE_APP_AUTH_OAUTH2_CLIENT_ID,
      redirectUri: process.env.VUE_APP_AUTH_OAUTH2_REDIRECT_URI,
      audience: process.env.VUE_APP_AUTH_OAUTH2_AUDIENCE
    }
  },
  platforms: {
    intercom: { id: process.env.VUE_APP_VUE_INTERCOM_ID },
    sentry: { dsn: process.env.VUE_APP_SENTRY_DSN }
  },
  mode: process.env.NODE_ENV
}
const config = RevasUI.configure(options)

const storeModules = {
  universo: universoStore(config.http)
}

RevasUI.install('universo', config, storeModules, routes)
