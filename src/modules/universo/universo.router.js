import VAppCollection from '@/modules/universo/views/VAppCollection'

export default [
  {
    path: 'apps',
    name: 'UNIVERSO.APP_COLLECTION.$NAME',
    component: VAppCollection,
    require: {
      realm: true
    }
  }
]
