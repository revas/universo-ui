import types from './universo.store.mutations'

const createUniversoStore = (endpoint) => {
  return {
    namespaced: true,
    state: {
      isWaiting: false,
      apps: []
    },
    getters: {
      isWaiting: (state) => {
        return state.isWaiting
      },
      apps: (state) => {
        return state.apps
      }
    },
    mutations: {
      [types.UNIVERSO_GET_APPS_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.UNIVERSO_GET_APPS_SUCCESS] (state, payload) {
        state.isWaiting = false
        state.apps = payload
      },
      [types.UNIVERSO_GET_APPS_FAILURE] (state) {
        state.isWaiting = false
      }
    },
    actions: {
      getApps ({ commit }) {
        try {
          commit(types.UNIVERSO_GET_APPS_REQUEST)
          const data = [
            {
              name: 'membro',
              title: 'UNIVERSO.APP_COLLECTION.MEMBRO_TITLE',
              description: 'UNIVERSO.APP_COLLECTION.MEMBRO_DESCRIPTION',
              nameOnline: 'members'
            },
            {
              name: 'stenografo',
              title: 'UNIVERSO.APP_COLLECTION.PUBLIKO_TITLE',
              description: 'UNIVERSO.APP_COLLECTION.PUBLIKO_DESCRIPTION',
              nameOnline: 'documents'
            }
          ]
          commit(types.UNIVERSO_GET_APPS_SUCCESS, data)
        } catch (e) {
          commit(types.UNIVERSO_GET_APPS_FAILURE)
          console.log(e)
        }
      }
    }
  }
}

export default createUniversoStore
